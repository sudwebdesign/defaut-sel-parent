<?php if (!defined('PLX_ROOT')) exit; if (eval($plxShow->callHook('baby::header'))) return; ?>
<!DOCTYPE html>
<html lang="<?php $plxShow->defaultLang() ?>">
<head>
	<meta charset="<?php $plxShow->charset('min'); ?>">
	<meta name="viewport" content="width=device-width, user-scalable=yes, initial-scale=1.0">
	<title><?php $plxShow->pageTitle(); ?></title>
	<?php $plxShow->meta('description') ?>
	<?php $plxShow->meta('keywords') ?>
	<?php $plxShow->meta('author') ?>
	<link rel="icon" href="<?php $plxShow->template(); ?>/img/favicon.png" />
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/plucss.css?v=210419" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/css/theme.css?v=190720" media="screen"/>
	<link rel="stylesheet" type="text/css" href="<?php $plxShow->template(); ?>/fonts/fontello/fontello.css?v=5.8.3" media="screen" />

	<?php $plxShow->templateCss() ?>
	<?php $plxShow->pluginsCss() ?>
	<?php $plxShow->callHook('baby::getcss') ?>
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" />
	<link rel="alternate" type="application/rss+xml" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>" href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires') ?>" />
</head>

<body id="top" class="page mode mode-<?php echo str_replace('-', ' ', $plxMotor->mode) ?>">

	<header class="header sticky">

		<div class="container">

			<div class="grid">

				<div class="col sml-12 my-menu">

					<div class="col sml-6 med-12">

						<div class="my-logo">

							<h1 class="no-margin heading-small med-float-left">
<!--
								<a href="<?php $plxShow->urlRewrite('index.php?my-art.html') ?>"
								class="sml-h3 lrg-h1 button green float-right med-offset-1"
								title="<?php $plxShow->lang('PUBLISH_TITLE') ?>"><?php $plxShow->lang('PUBLISH') ?></a>
-->
								<?php #$plxShow->callHook('adhesionSelPuplicBtn', array('name' => $plxShow->getLang('PUBLISH'), 'title' => $plxShow->getLang('PUBLISH_TITLE'), 'class' => 'sml-h3 lrg-h1 button green float-right med-offset-1')); ?>
								<?php #$plxShow->callHook('adhesionSelPuplicBtn', $plxShow->getLang('PUBLISH'), $plxShow->getLang('PUBLISH_TITLE'), 'sml-h3 lrg-h1 button green float-right med-offset-1'); ?>
								<?php eval($plxShow->callHook('adhesionSelPuplicBtn', array($plxShow->getLang('PUBLISH'), $plxShow->getLang('PUBLISH_TITLE'), 'sml-h3 lrg-h1 button green float-right med-offset-1'))); ?>
								<?php #eval($plxShow->callHook('adhesionSelPuplicBtn')); ?>
								<?php $plxShow->mainTitle('link'); ?>
							</h1>
							<h2 class="h5 no-margin med-float-right"><?php $plxShow->subTitle(); ?></h2>

						</div>

					</div>

					<div class="col sml-6 med-12">

						<nav class="nav">

							<div class="responsive-menu">
								<label for="menu"><?php #$plxShow->lang('MENU') ?></label>
								<input type="checkbox" id="menu" aria-label="menu">
								<ul class="menu">

	<!--
	<li id="categories" class="categories static menu noactive menu-item page_item menu-item-type-post_type menu-item-object-page menu-item-has-children">
	 <a href="#"><span class="menu-item static group noactive">THEMATIQUES</span></a>
	 <ul id="categories-list" class="sub-menu">
									<?php #$plxShow->catList($plxShow->getLang('HOME'),'<li class="#cat_status page_item"><a href="#cat_url" title="#cat_name">#cat_name</a></li>'); ?>
	 </ul>
	</li>
	-->
									<?php $plxShow->staticList($plxShow->getLang('HOME'),'<li class="#static_class #static_status" id="#static_id"><a href="#static_url" title="#static_name">#static_name</a></li>'); ?>
									<?php $plxShow->pageBlog('<li class="#page_class #page_status" id="#page_id"><a href="#page_url" title="#page_name">#page_name</a></li>'); ?>
								</ul>
							</div>

						</nav>

					</div>

				</div>
			</div>

		</div>

	</header>

	<div class="bg"></div>
