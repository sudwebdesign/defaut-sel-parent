<?php if (eval($plxShow->callHook('baby::template',array('static-full-width-sel-iscroll')))) return; include(dirname(__FILE__) . '/header.php'); ?>

	<main class="main">

		<div class="container">

			<div class="grid">

				<div class="content col sml-12">

					<div class="article static mode <?php echo str_replace('-', ' ', $plxMotor->mode) ?>" id="static-page-<?php echo $plxShow->staticId(); ?>">

						<header>
							<h2>
								<?php $plxShow->staticTitle(); ?>
							</h2>
						</header>

						<?php $plxShow->staticContent(); ?>

					</div>

				</div>

			</div>

		</div>

	</main>

<?php include(dirname(__FILE__).'/footer.php'); ?>

