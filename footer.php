<?php if (!defined('PLX_ROOT')) exit; if (eval($plxShow->callHook('baby::footer'))) return; ?>

	<footer class="footer">
		<div class="container">
			<p>
				<?php $plxShow->mainTitle('link'); ?> - <?php $plxShow->subTitle(); ?> &copy; 2020
			</p>
			<p>
				<?php $plxShow->lang('POWERED_BY') ?>&nbsp;<a href="http://www.pluxml.org" title="<?php $plxShow->lang('PLUXML_DESCRIPTION') ?>">PluXml</a>
				<?php $plxShow->lang('IN') ?>&nbsp;<?php $plxShow->chrono(); ?>
				<?php $plxShow->httpEncoding() ?>
			</p>
			<p>
				<a rel="nofollow" href="<?php $plxShow->urlRewrite('core/admin/'); ?>" title="<?php $plxShow->lang('ADMINISTRATION') ?>"><?php $plxShow->lang('ADMINISTRATION') ?></a>
<?php if(isset($_SESSION['account'])) {#est connecté ?>
				-&nbsp;<a rel="nofollow" href="<?php $plxShow->urlRewrite('?myaccount.html'); ?>" title="<?php $plxShow->plxMotor->plxPlugins->aPlugins['adhesion']->lang('L_MY_ACCOUNT') ?>"><?php $plxShow->plxMotor->plxPlugins->aPlugins['adhesion']->lang('L_MY_ACCOUNT') ?></a>
<?php } ?>
			</p>
			<ul class="menu">
				<li><a href="<?php $plxShow->urlRewrite('feed.php?rss') ?>" title="<?php $plxShow->lang('ARTICLES_RSS_FEEDS'); ?>"><?php $plxShow->lang('ARTICLES'); ?></a></li>
				<li><a href="<?php $plxShow->urlRewrite('feed.php?rss/commentaires'); ?>" title="<?php $plxShow->lang('COMMENTS_RSS_FEEDS') ?>"><?php $plxShow->lang('COMMENTS'); ?></a></li>
				<li><a href="<?php $plxShow->urlRewrite('#top') ?>" title="<?php $plxShow->lang('GOTO_TOP') ?>"><?php $plxShow->lang('TOP') ?></a></li>
			</ul>
		</div>
	</footer>
	<?php $plxShow->callHook('baby::getjs') ?>
</body>

</html>
